#!/bin/bash

echo 'Updating Ubuntu'
sudo apt-get -qq -y update > /dev/null 2>&1

echo 'Installing git'
sudo apt-get install -qq git > /dev/null 2>&1

echo 'Disabling swap'
sudo swapoff -a

# Comment the swap line from fstab - permanently disable swap
sudo sed -i.bak '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

