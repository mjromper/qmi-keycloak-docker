# KeyCloak IDP on Docker
## Description
A local demonstration environment for integration authentication with Qlik's products.

| Servername        | Server IP         | Purpose | 
|-------------------|-------------------|---------|
| qmi-keycloack-docker | 192.168.56.71   | Identity Provider (SAML & OIDC)|

## Hosts file
It is recommended that you add the following to both your laptops HOSTS file 

_192.168.56.71 qmi-keycloak qmi-keycloack-docker keycloak.qliklocal.net_

And to any servers that will authenticate against it

The hosts file is found here: __c:\windows\system32\drivers\etc__ on a windows box, or __/etc/hosts__ on a linux box.

## Linux Users
| Name | Password |
|------|-----|
|vagrant|vagrant|



## KeyCloak Identity Provider

The admin console can be accessed at:
http://192.168.56.71:8080

username: qlik
password: Qlik1234


## Connection
Please use __vagrant ssh__ to connect to the server (or if using your own SSH use ssh vagrant@keycloak.qliklocal.net to connect (password: vagrant))

## Purpose
This scenario provisions the keycloak identity provider pre-configured for Qlik Sense (qmi-qs-sn) and QSE on K8S (keycloak.elastic.example)

## What is installed
### Software
1. Ubuntu 18.04
2. Docker
2. Keycloak IDP on docker

### Support Information
| Author | Version | Date Published |
|--------|---------|----------------|
|Manuel Romero|1.0|2 April 2019|
